﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Malcanek.ExampleProjects.ShopApi.PublicModels
{
    /// <summary>
    /// Model containing details about a single product.
    /// </summary>
    public class ProductModel : IValidatableObject
    {
        /// <summary>
        /// Id of the product in the database.
        /// Cannot be modified by update.
        /// </summary>
        /// <example>0</example>
        public int Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// This attribute is always required.
        /// </summary>
        /// <example>Kingston FURY 32GB</example>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// URL of the product image.
        /// Must be a HTTPS.
        /// This attribute is always required.
        /// </summary>
        /// <example>https://cdn.shop.com/kingston_fury_32gb.png</example>
        [Required]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Price of the product.
        /// Must be a positive number.
        /// This attribute is always required.
        /// </summary>
        /// <example>3690</example>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// Description of the product.
        /// </summary>
        /// <example>RAM 2x16GB - Passive cooling</example>
        public string Description { get; set; }

        /// <summary>
        /// Date when the product was created.
        /// Can be null only when creating a new product.
        /// Cannot be modified by update.
        /// </summary>
        /// <example>2022-01-02T17:24:26.187Z</example>
        public DateTime? DateCreated { get; set; }

        /// <summary>
        /// Date when the product was deleted.
        /// If this date is null, then it's still active product.
        /// Cannot be modified by update.
        /// </summary>
        /// <example>2022-01-02T17:24:26.187Z</example>
        public DateTime? DateDeleted { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                yield return new ValidationResult("Name is required.", new[] { nameof(Name) });
            }

            if (string.IsNullOrWhiteSpace(ImageUrl))
            {
                yield return new ValidationResult("ImageUrl is required.", new[] { nameof(ImageUrl) });
            }

            if (!ImageUrl.Trim().ToLower().StartsWith("https://"))
            {
                yield return new ValidationResult("ImageUrl must be HTTPS.", new[] { nameof(ImageUrl) });
            }

            if (Price < 0)
            {
                yield return new ValidationResult("Price must be a positive number.", new[] { nameof(Price) });
            }
        }
    }
}
