﻿using System;

namespace Malcanek.ExampleProjects.ShopApi.PublicModels
{
    /// <summary>
    /// Model returned for explaining errors
    /// </summary>
    public class ErrorModel

    {
        /// <summary>
        /// Error message
        /// </summary>
        public string Message { get; set; }

        public static ErrorModel ExceptionToModel(Exception exception)
        {
            return new ErrorModel
            {
                Message = exception.Message
            };
        }
    }
}
