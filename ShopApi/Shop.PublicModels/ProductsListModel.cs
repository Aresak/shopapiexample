﻿using System.Collections.Generic;

namespace Malcanek.ExampleProjects.ShopApi.PublicModels
{
    /// <summary>
    /// Model containing a list of products
    /// </summary>
    public class ProductsListModel
    {
        /// <summary>
        /// The offset of the list as requested in API
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// The count of the list as requested in API
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Flag whether you can call the endpoint again for more products
        /// </summary>
        public bool HasMore { get; set; }

        /// <summary>
        /// List of the products
        /// </summary>
        public List<ProductModel> Products { get; set; }
    }
}
