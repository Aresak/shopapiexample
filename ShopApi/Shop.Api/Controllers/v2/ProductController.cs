﻿using Malcanek.ExampleProjects.ShopApi.PublicModels;
using Malcanek.ExampleProjects.ShopApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Malcanek.ExampleProjects.ShopApi.Api.Controllers.v2
{
    [ApiController]
    [Route("v2/products")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        /// <summary>
        /// Gets a list of products.
        /// Only products that aren't deleted.
        /// </summary>
        /// <param name="start">Offset of the list</param>
        /// <param name="count">Amount of products returned, default 10</param>
        /// <returns>List of the products</returns>
        [HttpPost("get/list/{start}/{count}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<ProductsListModel>> GetList(int start, int count = 10)
        {
            ProductsListModel list = await productService.GetList(start, count);

            if (list.Count > 0)
            {
                return Ok(list);
            }
            else
            {
                return NoContent();
            }
        }
    }
}