﻿using Malcanek.ExampleProjects.ShopApi.PublicModels;
using Malcanek.ExampleProjects.ShopApi.Services.Exceptions;
using Malcanek.ExampleProjects.ShopApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Malcanek.ExampleProjects.ShopApi.Api.Controllers.v1
{
    /// <summary>
    /// This endpoint group manages everything about products.
    /// </summary>
    [ApiController]
    [Route("v1/products")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        /// <summary>
        /// Create a new product
        /// </summary>
        /// <param name="product">Product model to be created. Id must be 0</param>
        /// <returns>The newly created product</returns>
        [HttpPut("create")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ProductModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        public async Task<IActionResult> CreateProduct([FromBody] ProductModel product)
        {
            try
            {
                ProductModel createdProduct = await productService.CreateProduct(product);

                return CreatedAtAction(nameof(Get), createdProduct.Id, createdProduct);
            }
            catch (ServiceException badRequest)
            {
                return BadRequest(ErrorModel.ExceptionToModel(badRequest));
            }
        }

        /// <summary>
        /// Updates name, description, image url or price of the product.
        /// </summary>
        /// <param name="product">Existing product to update</param>
        /// <returns>Updated product with latest information</returns>
        [HttpPatch("update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductModel product)
        {
            try
            {
                ProductModel updatedProduct = await productService.UpdateProduct(product);

                return Ok(updatedProduct);
            }
            catch (ServiceException badRequest)
            {
                return NotFound(ErrorModel.ExceptionToModel(badRequest));
            }
        }

        /// <summary>
        /// Delete an existing product
        /// </summary>
        /// <param name="product">Product to be deleted</param>
        /// <returns>Latest product information including the delete date</returns>
        [HttpDelete("delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        public async Task<IActionResult> DeleteProduct([FromBody] ProductModel product)
        {
            try
            {
                ProductModel deletedProduct = await productService.DeleteProduct(product);

                return Ok(deletedProduct);
            }
            catch (ServiceException badRequest)
            {
                return BadRequest(ErrorModel.ExceptionToModel(badRequest));
            }
        }

        /// <summary>
        /// Delete an existing product
        /// </summary>
        /// <param name="id">Product's ID to be deleted</param>
        /// <returns>Latest product information including the delete date</returns>
        [HttpDelete("delete/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                ProductModel deletedProduct = await productService.DeleteProduct(id);

                return Ok(deletedProduct);
            }
            catch(ServiceException badRequest)
            {
                return BadRequest(ErrorModel.ExceptionToModel(badRequest));
            }
        }

        /// <summary>
        /// Get a product.
        /// Used to get product information even if it's deleted.
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <returns>Latest product information</returns>
        [HttpGet("get/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<ProductModel>> Get(int id)
        {
            ProductModel product = await productService.Get(id);

            if (product != null)
            {
                return Ok(product);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// Gets a list of products.
        /// Used to get products information even if it's deleted.
        /// </summary>
        /// <param name="ids">Ids of the products</param>
        /// <returns>Latest products informations</returns>
        [HttpPost("get/list/ids")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<ProductsListModel>> Get([FromBody] int[] ids)
        {
            ProductsListModel list = await productService.GetList(ids);

            if (list.Count > 0)
            {
                return Ok(list);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// Gets a list of products.
        /// Only those that aren't deleted.
        /// </summary>
        /// <returns>Latest products informations</returns>
        [HttpPost("get/list")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<ProductsListModel>> Get()
        {
            ProductsListModel list = await productService.GetList();

            if (list.Count > 0)
            {
                return Ok(list);
            }
            else
            {
                return NoContent();
            }
        }
    }
}