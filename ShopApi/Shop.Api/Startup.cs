using Malcanek.ExampleProjects.ShopApi.Context;
using Malcanek.ExampleProjects.ShopApi.Services;
using Malcanek.ExampleProjects.ShopApi.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.IO;

namespace Malcanek.ExampleProjects.ShopApi.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            SetupSwagger(services);
            SetupDatabase(services);

            services.AddTransient<IProductService, ProductService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ShopContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Shop v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            context.Database.Migrate();
        }

        private void SetupDatabase(IServiceCollection services)
        {
            string server = Configuration.GetConnectionString("SystemDatabase");
            server = server.Replace("%db_file%", Configuration.GetConnectionString("DatabaseFile"));

            services.AddDbContext<ShopContext>(options => options.UseSqlServer(server), ServiceLifetime.Scoped, ServiceLifetime.Singleton);
        }

        private void SetupSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Shop", Version = "v1" });

                c.IncludeXmlComments(Path.Combine(Configuration["DocumentationFolder"], "Malcanek.ExampleProjects.ShopApi.Api.xml"), includeControllerXmlComments: true);
                c.IncludeXmlComments(Path.Combine(Configuration["DocumentationFolder"], "Malcanek.ExampleProjects.ShopApi.PublicModels.xml"), includeControllerXmlComments: true);
                c.IncludeXmlComments(Path.Combine(Configuration["DocumentationFolder"], "Malcanek.ExampleProjects.ShopApi.Services.xml"), includeControllerXmlComments: true);
            });
        }
    }
}
