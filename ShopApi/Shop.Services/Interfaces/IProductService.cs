﻿using Malcanek.ExampleProjects.ShopApi.PublicModels;
using System.Threading.Tasks;

namespace Malcanek.ExampleProjects.ShopApi.Services.Interfaces
{
    public interface IProductService
    {
        Task<ProductModel> CreateProduct(ProductModel product);
        
        Task<ProductModel> UpdateProduct(ProductModel product);

        Task<ProductModel> DeleteProduct(ProductModel product);

        Task<ProductModel> DeleteProduct(int id);

        Task<ProductModel> Get(int id);

        Task<ProductsListModel> GetList(int[] ids);

        Task<ProductsListModel> GetList();

        Task<ProductsListModel> GetList(int start, int count);
    }
}
