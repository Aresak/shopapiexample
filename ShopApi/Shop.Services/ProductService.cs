﻿using Malcanek.ExampleProjects.ShopApi.Context;
using Malcanek.ExampleProjects.ShopApi.Context.DatabaseModels;
using Malcanek.ExampleProjects.ShopApi.PublicModels;
using Malcanek.ExampleProjects.ShopApi.Services.Exceptions;
using Malcanek.ExampleProjects.ShopApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Malcanek.ExampleProjects.ShopApi.Services
{
    public class ProductService : IProductService
    {
        private readonly ShopContext context;

        public ProductService(ShopContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Creates a product
        /// </summary>
        /// <param name="product">Product to create</param>
        /// <returns>Newly created product</returns>
        public async Task<ProductModel> CreateProduct(ProductModel product)
        {
            if (product.Id == 0)
            {
                product.Name = product.Name.Trim();
                product.Description = product.Description.Trim();

                Product dbProduct = new()
                {
                    Price = product.Price,
                    Description = product.Description,
                    Name = product.Name,
                    DateCreated = DateTime.UtcNow,
                    DateDeleted = null,
                    ImageUrl = product.ImageUrl
                };

                await context.Product.AddAsync(dbProduct);
                await context.SaveChangesAsync();

                product = MapModel(dbProduct);
            }
            else
            {
                throw new ServiceException("When creating a product, the Id must be 0");
            }

            return product;
        }

        /// <summary>
        /// Deletes a product
        /// </summary>
        /// <param name="product">Product to delete</param>
        /// <returns>Deleted product's information</returns>
        public async Task<ProductModel> DeleteProduct(ProductModel product)
        {
            product = await DeleteProduct(product.Id);

            return product;
        }

        /// <summary>
        /// Deletes a product
        /// </summary>
        /// <param name="id">Product's Id to delete</param>
        /// <returns>Deleted product's information</returns>
        public async Task<ProductModel> DeleteProduct(int id)
        {
            ProductModel product = null;
            Product dbProduct = await context.Product.FirstOrDefaultAsync(dbProduct => dbProduct.Id == id);

            if(dbProduct == null)
            {
                throw new ServiceException($"Product {id} wasn't found");
            }

            if(dbProduct.DateDeleted != null)
            {
                throw new ServiceException("Product already deleted");
            }

            dbProduct.DateDeleted = DateTime.UtcNow;
            await context.SaveChangesAsync();

            product = MapModel(dbProduct);

            return product;
        }

        /// <summary>
        /// Get a Product.
        /// Including deleted products.
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <returns>Fetched product</returns>
        public async Task<ProductModel> Get(int id)
        {
            return MapModel(await context.Product
                .AsNoTracking()
                .FirstOrDefaultAsync(dbProduct => dbProduct.Id == id));
        }

        /// <summary>
        /// Get a list of products.
        /// Including deleted products.
        /// </summary>
        /// <param name="ids">List of product Ids</param>
        /// <returns>List of fetched products</returns>
        public async Task<ProductsListModel> GetList(int[] ids)
        {
            List<int> idList = ids.ToList();
            ProductsListModel productsList = new()
            {
                Start = 0,
                HasMore = false
            };

            productsList.Products = await context.Product
                .AsNoTracking()
                .Where(dbProduct => idList.Contains(dbProduct.Id))
                .Select(dbProduct => MapModel(dbProduct))
                .ToListAsync();

            productsList.Count = productsList.Products.Count;

            return productsList;
        }

        /// <summary>
        /// Get a list of products.
        /// Excludes deleted products.
        /// </summary>
        /// <returns>List of all products</returns>
        public async Task<ProductsListModel> GetList()
        {
            ProductsListModel productsList = new()
            {
                Start = 0,
                HasMore = false
            };

            productsList.Products = await context.Product
                .AsNoTracking()
                .Where(dbProduct => dbProduct.DateDeleted == null)
                .Select(dbProduct => MapModel(dbProduct))
                .ToListAsync();

            productsList.Count = productsList.Products.Count;

            return productsList;
        }

        /// <summary>
        /// Get a list of products.
        /// Excludes deleted products.
        /// </summary>
        /// <param name="start">Offset in the list</param>
        /// <param name="count">Count of the products</param>
        /// <returns>List of products in the range</returns>
        public async Task<ProductsListModel> GetList(int start, int count)
        {
            ProductsListModel productsList = new()
            {
                Start = start,
                Count = count
            };

            productsList.Products = await context.Product
                .AsNoTracking()
                .Where(dbProduct => dbProduct.DateDeleted == null)
                .Skip(start)
                .Take(count)
                .Select(dbProduct => MapModel(dbProduct))
                .ToListAsync();

            int totalProducts = await context.Product.CountAsync();

            productsList.HasMore = (start + count) < totalProducts;

            return productsList;
        }

        /// <summary>
        /// Updates the product.
        /// Updateable properties:
        /// - Name
        /// - Description
        /// - ImageUrl
        /// - Price
        /// </summary>
        /// <param name="product">The product model to update</param>
        /// <returns>Updated product</returns>
        public async Task<ProductModel> UpdateProduct(ProductModel product)
        {
            if (product.Id != 0)
            {
                Product dbProduct = await context.Product.FirstOrDefaultAsync(dbProduct => dbProduct.Id == product.Id);

                if (dbProduct != null)
                {
                    dbProduct.Name = product.Name?.Trim();
                    dbProduct.Description = product.Description?.Trim();
                    dbProduct.ImageUrl = product.ImageUrl?.Trim();
                    dbProduct.Price = product.Price;

                    await context.SaveChangesAsync();
                    product = MapModel(dbProduct);
                }
                else
                {
                    throw new ServiceException("Failed to find product");
                }
            }
            else
            {
                throw new ServiceException("Product Id not specified");
            }

            return product;
        }

        /// <summary>
        /// Maps a database product to public model
        /// </summary>
        /// <param name="product">Database product</param>
        /// <returns>Product public model</returns>
        public static ProductModel MapModel(Product product)
        {
            ProductModel model = null;

            if (product != null)
            {
                model = new ProductModel
                {
                    Id = product.Id,
                    DateCreated = product.DateCreated,
                    DateDeleted = product.DateDeleted,
                    Description = product.Description,
                    ImageUrl = product.ImageUrl,
                    Name = product.Name,
                    Price = product.Price
                };
            }

            return model;
        }
    }
}
