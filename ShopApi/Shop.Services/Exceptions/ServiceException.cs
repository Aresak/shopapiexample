﻿using System;

namespace Malcanek.ExampleProjects.ShopApi.Services.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException(string message) : base(message)
        {

        }
    }
}
