﻿using Malcanek.ExampleProjects.ShopApi.Context.DatabaseModels;
using System;
using System.Text;

namespace Malcanek.ExampleProjects.ShopApi.Services.Tests.ContextMocks
{
    public static class RandomProductMock
    {
        private static readonly DateTime BaseDateTime = new(2022, 1, 1);

        public static Product GenerateRandomProduct(int id, Random random)
        {
            string name = GenerateName(random);
            string imageUrl = GenerateImageUrl(random);
            decimal price = GeneratePrice(random);
            string description = GenerateDescription(random);
            DateTime dateCreated = GenerateDateCreated(random);
            DateTime? dateDeleted = GenerateDateDeleted(random, id, dateCreated);

            return new Product
            {
                Id = id,
                Name = name,
                ImageUrl = imageUrl,
                Price = price,
                Description = description,
                DateCreated = dateCreated,
                DateDeleted = dateDeleted
            };
        }

        private static string GenerateName(Random random)
        {
            string[] mainParts = new string[]
            {
                "Prebuilt PC",
                "Keyboard",
                "Mouse",
                "PC Set",
                "Monitor FullHD",
                "Monitor HD",
                "Speakers 5.1",
                "Speakers 2.1",
                "Speakers 2.0",
                "Motherboard",
                "Robot Phone",
                "iFruit Phone",
                "Headphones",
                "Lawnmower",
                "Garden pump",
                "SmartHome Set"
            };

            string[] midParts = new string[]
            {
                "PogChamp",
                "Kappa",
                "LUL",
                "Perfection",
                "Home",
                "Family",
                "Enterprise",
                "Ultimate"
            };

            string[] versionParts = new string[]
            {
                "3.0",
                "1.0",
                "3000",
                "FINAL",
                "Final-FINAL",
                "Final-FINAL-FINAL",
                "Final-FINAL-FINAL2",
                "6.80",
                "Cat",
                "Dog",
                "PRO",
                "Lite",
                "Free"
            };

            string mainPart = mainParts[random.Next(0, mainParts.Length - 1)];
            string midPart = midParts[random.Next(0, midParts.Length - 1)];
            string versionPart = versionParts[random.Next(0, versionParts.Length - 1)];

            return $"{mainPart} {midPart} {versionPart}";
        }

        private static string GenerateImageUrl(Random random)
        {
            string[] domains = new string[]
            {
                "freeworld.com",
                "main.cdn.com",
                "freeworld.cdn.com",
                "free.cdn.com",
                "unlimited.cdn.com"
            };

            string[] extensions = new string[]
            {
                "jpeg",
                "jpg",
                "png",
                "gif"
            };

            string domain = domains[random.Next(0, domains.Length - 1)];
            string item = random.Next(0, 999999).ToString();
            string extension = extensions[random.Next(0, extensions.Length - 1)];

            string url = $"https://{domain}/{item}.{extension}";

            return url;
        }

        private static decimal GeneratePrice(Random random)
        {
            return random.Next(10, 100000) / 10;
        }

        private static string GenerateDescription(Random random)
        {
            string lorem_ipsum_base = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, "
                + "totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. "
                + "Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, "
                + "qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, "
                + "adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. "
                + "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi "
                + "consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel"
                + "illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] At vero eos et accusamus et iusto odio dignissimos ducimus,"
                + "qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati"
                + "cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. "
                + "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque "
                + "nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. "
                + "Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae "
                + "sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores "
                + "alias consequatur aut perferendis doloribus asperiores repellat";


            string[] lorem_ipsum_array = lorem_ipsum_base.Split(' ');

            StringBuilder description_builder = new();

            for (int word_index = 0; word_index < 36; word_index++)
            {
                string word = lorem_ipsum_array[random.Next(0, lorem_ipsum_array.Length - 1)];

                description_builder.Append($"{word} ");
            }

            return description_builder.ToString().Trim();
        }

        private static DateTime GenerateDateCreated(Random random)
        {
            return BaseDateTime.AddDays(random.Next(0, 90) * -1);
        }

        private static DateTime? GenerateDateDeleted(Random random, int id, DateTime dateCreated)
        {
            DateTime? dateDeleted = null;

            if(id != 1)
            {
                bool isDeleted = id == 2 || random.Next(5) == 0;

                if (isDeleted)
                {
                    int daysDiff = (int)(BaseDateTime - dateCreated).TotalDays;
                    dateDeleted = dateCreated.AddDays(random.Next(0, daysDiff));
                }
            }

            return dateDeleted;
        }
    }
}
