﻿using Malcanek.ExampleProjects.ShopApi.Context;
using Malcanek.ExampleProjects.ShopApi.Context.DatabaseModels;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Malcanek.ExampleProjects.ShopApi.Services.Tests.ContextMocks
{
    public static class ShopContextMock
    {
        public static Mock<ShopContext> CreateContextMock()
        {
            Mock<ShopContext> contextMock = new();

            CreateProductsMock(contextMock);

            return contextMock;
        }

        private static void CreateProductsMock(Mock<ShopContext> contextMock)
        {
            Mock<DbSet<Product>> productsMock = new();

            List<Product> productsData = CreateProductsMockData();

            contextMock.Setup(context => context.Product).ReturnsDbSet(productsData);
        }

        private static List<Product> CreateProductsMockData()
        {
            List<Product> products = new();

            Random random = new(100);

            for (int id = 1; id < 150; id++)
            {
                Product generatedProduct = RandomProductMock.GenerateRandomProduct(id, random);
                products.Add(generatedProduct);
            }

            return products;
        }
    }
}
