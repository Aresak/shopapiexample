﻿using Malcanek.ExampleProjects.ShopApi.Context;
using Malcanek.ExampleProjects.ShopApi.PublicModels;
using Malcanek.ExampleProjects.ShopApi.Services.Exceptions;
using Malcanek.ExampleProjects.ShopApi.Services.Tests.ContextMocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Malcanek.ExampleProjects.ShopApi.Services.Tests
{
    [TestClass]
    public class ProductServiceTests
    {
        [TestMethod]
        public async Task CreateProductWithoutId()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = new()
            {
                Name = "Product 1",
                Description = "Product description",
                ImageUrl = "https://asd.com/img.png",
                Price = 10.66M,
                DateCreated = DateTime.UtcNow.AddDays(-1),
                DateDeleted = DateTime.UtcNow
            };

            ProductModel product2 = await productService.CreateProduct(product);

            Assert.AreEqual(product.Name, product2.Name);
            Assert.AreEqual(product.Description, product2.Description);
            Assert.AreEqual(product.ImageUrl, product2.ImageUrl);
            Assert.AreEqual(product.Price, product2.Price);
            Assert.AreNotEqual(product.DateCreated, product2.DateCreated);
            Assert.AreNotEqual(product.DateDeleted, product2.DateDeleted);
        }

        [TestMethod]
        [ExpectedException(typeof(ServiceException))]
        public async Task CreateProductWithId()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = new()
            {
                Id = 1,
                Name = "Product 1",
                Description = "Product description",
                ImageUrl = "https://asd.com/img.png",
                Price = 10.66M,
                DateCreated = DateTime.UtcNow.AddDays(-1),
                DateDeleted = DateTime.UtcNow
            };


            await productService.CreateProduct(product);
        }

        [TestMethod]
        public async Task DeleteProduct()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = new()
            {
                Id = 1
            };

            ProductModel product2 = await productService.DeleteProduct(product);

            Assert.IsNotNull(product2.DateDeleted);
        }

        [TestMethod]
        public async Task DeleteProductById()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product2 = await productService.DeleteProduct(1);

            Assert.IsNotNull(product2.DateDeleted);
        }

        [TestMethod]
        [ExpectedException(typeof(ServiceException))]
        public async Task DeleteProductWithDateDeleted()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = new()
            {
                Id = 2
            };

            await productService.DeleteProduct(product);
        }

        [TestMethod]
        [ExpectedException(typeof(ServiceException))]
        public async Task DeleteProductWithoutId()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = new()
            {
                Id = -1
            };

            await productService.DeleteProduct(product);
        }

        [TestMethod]
        public async Task GetProduct()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = await productService.Get(20);

            Assert.AreEqual(product.Id, 20);
        }

        [TestMethod]
        public async Task GetProductsListByIds()
        {
            ProductService productService = GetProductService(GetContext());

            int[] requestedIds = new int[] { 2, 4, 6, 8, 10, 12 };

            ProductsListModel list = await productService.GetList(requestedIds);

            Assert.AreEqual(list.Count, requestedIds.Length);
            Assert.AreEqual(list.Products[0].Id, requestedIds[0]);
            Assert.AreEqual(list.Products[1].Id, requestedIds[1]);
            Assert.AreEqual(list.Products[2].Id, requestedIds[2]);
            Assert.AreEqual(list.Products[3].Id, requestedIds[3]);
            Assert.AreEqual(list.Products[4].Id, requestedIds[4]);
            Assert.AreEqual(list.Products[5].Id, requestedIds[5]);
        }

        [TestMethod]
        public async Task GetProductsAll()
        {
            ProductService productService = GetProductService(GetContext());

            ProductsListModel list = await productService.GetList();

            foreach(ProductModel product in list.Products)
            {
                Assert.IsNull(product.DateDeleted);
            }
        }

        [TestMethod]
        public async Task GetProductsByRange()
        {
            ProductService productService = GetProductService(GetContext());

            ProductsListModel list1 = await productService.GetList(0, 20);

            Assert.AreEqual(list1.Count, 20);
            Assert.AreEqual(list1.Products.Count, 20);
            Assert.AreEqual(list1.Start, 0);
            Assert.IsTrue(list1.HasMore);

            ProductsListModel list2 = await productService.GetList(20, 500);

            Assert.AreEqual(list2.Start, 20);
            Assert.AreEqual(list2.Count, 500);
            Assert.IsFalse(list2.HasMore);
        }

        [TestMethod]
        public async Task UpdateProduct()
        {
            ProductService productService = GetProductService(GetContext());

            ProductModel product = await productService.Get(1);

            ProductModel updateProduct = new()
            {
                Id = product.Id,
                Name = "test",
                Description = "test",
                ImageUrl = "test",
                Price = 1.11M,
                DateCreated = null,
                DateDeleted = DateTime.UtcNow
            };

            ProductModel updatedProduct = await productService.UpdateProduct(updateProduct);

            Assert.AreEqual(updatedProduct.Id, product.Id);
            Assert.AreEqual(updatedProduct.DateCreated, product.DateCreated);
            Assert.AreEqual(updatedProduct.DateDeleted, product.DateDeleted);
            Assert.AreSame(updatedProduct.Name, "test");
            Assert.AreSame(updatedProduct.Description, "test");
            Assert.AreSame(updatedProduct.ImageUrl, "test");
            Assert.AreEqual(updatedProduct.Price, 1.11M);
        }

        private static ShopContext GetContext()
        {
            return ShopContextMock.CreateContextMock().Object;
        }

        private static ProductService GetProductService(ShopContext context)
        {
            return new ProductService(context);
        }
    }
}
