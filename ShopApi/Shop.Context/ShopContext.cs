﻿using Malcanek.ExampleProjects.ShopApi.Context.DatabaseModels;
using Microsoft.EntityFrameworkCore;

namespace Malcanek.ExampleProjects.ShopApi.Context
{
    public class ShopContext : DbContext
    {
        public virtual DbSet<Product> Product { get; set; }

        public ShopContext()
        {

        }

        public ShopContext(DbContextOptions<ShopContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
