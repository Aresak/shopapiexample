# Shop API Example

Includes a solution for a simple API to manipulate with a products in a shop.

Project doesn't include any authorization, anyone can implement it's own.

## Requirements

To run the solution you just need .NET 5.0 installed on the device.

## How to run the API

In the Shop.Api/appsettings configure the following:

1. Database connection string in the ConnectionStrings.SystemDatabase
2. If you are using local MDF, edit the DatabaseFile to target your MDF
3. Select a directory containing XML documentation in DocumentationFolder (by default it is where your .sln file is)

Start IIS Express in a Visual Studio.

### Azure Deployment

#### Create a pipeline with the following tasks:

1. Use .NET Core 5.0
2. NuGet tool installer
3. NuGet - add path to solution
4. Visual Studio Build (see configuration bellow)
5. Publish build artifacts (path: "$(Build.ArtifactStagingDirectory)", artifact name: "drop", publish location: "Azure Pipelines")

##### Configuration for pipeline Visual Studio Build task

1. Add path to solution
2. Platform: "any cpu"
3. Confuguration: "release"
4. MSBuild Arguments:

```
/p:DeployOnBuild=true /p:WebPublishMethod=Package /p:PackageAsSingleFile=true /p:SkipInvalidConfigurations=true /p:PackageLocation="$(build.artifactstagingdirectory)\\"
```

#### Create a release with the created artifact in the "drop" folder.

Add any deployment target by your choice.

## How to run service tests

In Visual Studio go to Test tab and click "Run All Tests" (also possible with CTRL+R+A shortcut).